import numpy as np
from scipy.spatial import distance_matrix

class Kernel:
    """
    Base class for Kernels.

    Children must define
      - kernel
    """
    def __init__(self, input_dim=None):
        self.input_dim = input_dim
        self.kernels = [[self]]

    def __add__(self, other):
        self.kernels.append([other]) 
        return self

    def __mul__(self, other):
        self.kernels = [[self, other]]
        return self

    def __call__(self, x, y):
        K = np.zeros(shape=(x.shape[0], y.shape[0]), dtype=np.float)
        for product_kernels in self.kernels:
            K_prod = np.ones(shape=(x.shape[0], y.shape[0]), dtype=np.float)
            for p in product_kernels:
                if p.input_dim is not None:
                    d = p.input_dim
                    K_prod *= p.kernel(x[:, d:d+1], y[:, d:d+1])
                else:    
                    K_prod *= p.kernel(x, y)
            K += K_prod
            
        return K

class RBF(Kernel):
    def __init__(self, lengthscale, variance, **kwargs):
        self.lengthscale = lengthscale
        self.variance = variance
        super().__init__(**kwargs)

    def kernel(self, x, y):
        kernel = self.variance * np.exp(
            -np.square(distance_matrix(x, y))/ (2 * self.lengthscale**2))
        return kernel

class RationalQuadratic(Kernel):
    def __init__(self, lengthscale, variance, alpha, **kwargs):
        self.lengthscale = lengthscale
        self.variance = variance
        self.alpha = alpha
        super().__init__(**kwargs)

    def kernel(self, x, y):
        kernel = self.variance * np.power(1 +
            np.square(distance_matrix(x, y))/ (2 * self.alpha * self.lengthscale**2), -self.alpha)
        return kernel


class Periodic(Kernel):
    def __init__(self, variance, lengthscale, period, **kwargs):
        self.variance = variance
        self.lengthscale = lengthscale
        self.period = period
        super().__init__(**kwargs)

    def kernel(self, x, y):
        dm = distance_matrix(x, y, p=1)
        return self.variance * np.exp(
            -2 * np.square(np.sin(np.pi * dm / self.period)) / self.lengthscale**2)

class Matern32(Kernel):
    def __init__(self, lengthscale, variance, **kwargs):
        self.lengthscale = lengthscale
        self.variance = variance
        super().__init__(**kwargs)

    def kernel(self, x, y):
        distance = np.square(distance_matrix(x, y))
        kernel = self.variance * (1 + np.sqrt(3) * distance / self.lengthscale
            ) * np.exp(- np.sqrt(3) * distance / self.lengthscale)
        return kernel

class Linear(Kernel):
    def __init__(self, variance, bias, **kwargs):
        self.variance = variance
        self.bias = bias
        super().__init__(**kwargs)

    def kernel(self, x, y):
        return self.bias + self.variance * x.dot(y.T) 

class Noise(Kernel):
    """
    Noise kernel with the Kronecker delta

    $k(x, y) = \delta_{ij}$
    """
    FTOL = 1e-10
    def __init__(self, variance, **kwargs):
        self.variance = variance
        super().__init__(**kwargs)

    def kernel(self, x, y):
        kernel = self.variance * (distance_matrix(x, y) < self.FTOL)
        return kernel
