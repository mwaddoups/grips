import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
from tqdm import tqdm

class GPException(Exception):
    pass

class GP:
    def __init__(self, kernel, normalize=True):
        self.kernel = kernel
        self.normalize = normalize
        self.reset()

    def reset(self):
        self.x = None
        self.y = None
        self.y_mean = None
        self.y_std = None
        self.K_xx = None
        self.K_xx_inv = None

    def fit(self, x, y, noise_variance=None):
        if noise_variance is None:
            noise_variance = np.zeros(len(x))
        self.x = x
        if self.normalize:
            self.y_mean = np.mean(y)
            self.y_std = np.std(y)
        else:
            self.y_mean = 0
            self.y_std = 1
        self.y = (y - self.y_mean) / self.y_std

        self.K_xx = self.kernel(x, x) + np.diag(noise_variance.flatten())
        L_inv = np.linalg.inv(np.linalg.cholesky(self.K_xx))
        self.K_xx_inv = L_inv.T.dot(L_inv)

    def predict(self, new_x, noise_variance=None):
        if self.x is None:
            return np.zeros(new_x.shape[0], dtype=np.float).reshape(-1, 1), self.kernel(new_x, new_x)
        if noise_variance is None:
            noise_variance = np.zeros(len(new_x))

        K_x_newx = self.kernel(self.x, new_x)
        K_newx_newx = self.kernel(new_x, new_x) + np.diag(noise_variance.flatten())
        predictor = K_x_newx.T.dot(self.K_xx_inv)
        mu = predictor.dot(self.y)
        cov = K_newx_newx - predictor.dot(K_x_newx)
        return mu * self.y_std  + self.y_mean, cov * self.y_std**2

    def sample(self, new_x, noise_variance=None):
        if noise_variance is None:
            noise_variance = np.zeros(len(new_x))
        if self.x is None:
            mu = np.zeros(len(new_x))
            cov = self.kernel(new_x, new_x) + np.diag(noise_variance.flatten())
        else:
            mu, cov = self.predict(new_x, noise_variance)
            mu = mu.flatten()

        return np.random.multivariate_normal(mu, cov).reshape(-1, 1)

    def plot(self, xlim=None, num_points=1000):
        if xlim is None:
            x_min, x_max = (min(self.x), max(self.x))
        else:
            x_min, x_max = xlim
        x = np.linspace(x_min, x_max, num_points).reshape(-1, 1)
        mu, cov = self.predict(x)
        mu = mu.flatten()
        stds = np.sqrt(np.diag(cov) + 1e-10).flatten()
        plt.figure(figsize=(15,10))
        plt.plot(x, mu, label='mean')
        plt.scatter(self.x, self.y, marker='x', label='data')
        plt.fill_between(x.flatten(), mu-stds, mu+stds, color='green', alpha=0.3, label='confidence')
        plt.legend()
        plt.show()

    def plot_residuals(self, sequential=False):
        residuals, errors = self.calc_residuals(sequential)
        rmse = np.sqrt(np.mean(np.square(errors)))
        fig, ax = plt.subplots(1, 1, figsize=(8, 5))
        ax.set_title(f'Residuals, mean = {np.mean(residuals):.3f}, rmse = {rmse:.3f}')
        ax.hist(residuals, bins=len(self.x) // 5, density=True)
        x = np.linspace(-3, 3)
        ax.plot(x, norm.pdf(x))
        plt.show()

    def calc_residuals(self, sequential=False):
        residuals = []
        errors = []
        x = self.x
        y = self.y
        for i in range(1, len(x)):
            if sequential:
                ix = np.arange(len(x)) < i
            else:
                ix = np.arange(len(x)) != i
            curr_x = x[ix]
            curr_y = y[ix]
            self.fit(curr_x, curr_y)
            mu, cov = self.predict(x[i, :].reshape(-1, 1))
            error = y[i].item() - mu.item()
            errors.append(error)
            residual = error / np.sqrt(cov.item())
            residuals.append(residual)
            
        self.fit(x, y)
        return residuals, errors 

